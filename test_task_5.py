"""Для таблицы «материала» создайте пользовательскую агрегатную функцию,
которая считает среднее значение весов всех материалов результирующей выборки
и округляет данной значение до целого.

Для таблицы «материала» создайте пользовательскую функцию,
которая принимает неограниченное количество полей и возвращает их конкатенацию.
"""
import sqlite3 as sq

materials = [
            ('Diamond', 0.5, 'Flawless'),
            ('Topaz', 0.26, 'Regular'),
            ('Sapphire', 1, 'Flawed'),
            ('Ruby', 20, 'Chipped')
]


class Average:
    def __init__(self):
        self.rows = []

    def step(self, value):
        self.rows.append(value)

    def finalize(self):
        avg_w = int(sum(self.rows) / len(self.rows))
        return avg_w


lst = []


def concatenate(*value):
    for i in value:
        lst.append(str(i))
    return ' '.join(lst)  # concatenate all fields


with sq.connect('materials.db') as conn:
    conn.create_aggregate('avg_weight', 1, Average)
    conn.create_function('conc', -1, concatenate)
    curs = conn.cursor()
    curs.execute('DROP TABLE IF EXISTS materials')
    curs.execute("""CREATE TABLE IF NOT EXISTS materials (
                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                    material TEXT NOT NULL,
                    weight_mg REAL DEFAULT 0,
                    quality TEXT
                )""")
    curs.executemany('INSERT INTO materials VALUES (NULL, ?, ?, ?)', materials)

curs.execute('SELECT avg_weight(weight_mg) FROM materials')
res = curs.fetchall()
print(f'Average weight of all materials = {res[0][0]}mg')
print()
curs = curs.execute('SELECT conc(material, weight_mg, quality) FROM materials')
row = curs.fetchall()
print(f'Concatenation 1: {row[curs.rowcount]}')
print(f'Concatenation 2: {"".join(row[len(row)-1])}')
# print(f'Concatenation 3: {" ".join(str_)}')
"""Сделать таблицу для подсчёта личных расходов со следующими полями: id, назначение, сумма, время.

- Создать консольный интерфейс (CLI) на Python для добавления новых записей в базу данных.

- Создать агрегатные функции для подсчёта общего количества расходов и расходов за месяц.
Обеспечить соответствующий интерфейс для пользователя.

- Измените таблицу так, чтобы можно было добавить не только расходы, а и доходы.

- Замените назначение на MCC и используйте его для определения назначения платежа
"""
import sqlite3 as sq
from sqlite3 import Error
from datetime import datetime, timezone

now = datetime.now(timezone.utc).astimezone()
time_format = '%Y-%m-%d %H:%M:%S'
date = f'{now:{time_format}}'
tip1 = 'Payment (Оплата): '
tip2 = 'MCC, 4 digits (Статья расходов, 4 цифры): '
tip3 = 'Top up account (Пополнение счета): '
tip4 = """        WARNING!
        Transaction was CANCELED: NOT ENOUGH MONEY ON YOUR BALANCE!
        Fund your bank account. (Псс, дружище... Денег на счет закинь, а...)
        """
tip5 = """
        It seems that such a TABLE(s) does not exists. 
        Maybe you\'ll try to create it first.
        Select first item [1] in 'Main menu'"""
tip6 = 'Input number of month (01, 2, 12..., etc): '


def db_create():
    """Create DB: if old DB exists --> it will be erased and will create new empty DB
    initial VALUES:
    id = 1
    assignment(MCC) = '0000' as TEXT
    credit = 15000 as INTEGER
    """
    curs.execute('DROP TABLE IF EXISTS balance')
    curs.execute("""CREATE TABLE IF NOT EXISTS balance (
                            id INTEGER PRIMARY KEY AUTOINCREMENT,
                            assignment TEXT,
                            datetime TEXT,
                            credit BIGINT DEFAULT 0,
                            debit BIGINT DEFAULT 0,
                            total BIGINT
                        )""")
    print('First transaction: You gained 15000$')
    curs.execute(f'INSERT INTO balance (assignment, total) VALUES (?,?)',
                 ('0000', 15000))


def tab_names():
    """Return to console names of all table columns and count of all columns"""
    curs.execute('PRAGMA table_info("balance")')
    column_names = [i[1] for i in curs.fetchall()]
    print(*column_names)
    print(len(column_names))


def set_debit():
    """Input values of payment and MCC into 'TABLE'
    val = money INTEGER
    mcc = MCC(4-digits code) TEXT
    :return: TABLE with VALUES(
    id = AUTOINCREMENT,
    assignment = mcc,
    datetime = current date and time,
    credit = 0,
    debit = val,
    total = total(prev) - debit
    """
    while True:
        try:
            val = int(input(tip1))
        except ValueError:
            print('Must be an integer or not empty')
        else:
            break
    while True:
        try:
            mcc = input(tip2)
            if len(mcc) != 4 or not mcc:
                print('Must contain 4 digits')
                raise ValueError
            if mcc == '0000':
                print('Must contain 4 digits, starting with 0001')
                raise ValueError
            if int(mcc):
                mcc = str(mcc)
        except ValueError:
            print('I said: FOUR DIGITS!!')
        else:
            break

    try:
        curs.execute(f'INSERT INTO balance (assignment, datetime, debit) VALUES (?,?,?)', (mcc, date, val))
        if 1 <= int(mcc) < 1500:
            print('Payment for Agricultural Services')
        elif 1500 <= int(mcc) < 3000:
            print('Payment for Contracted Services')
        elif 3000 <= int(mcc) < 3300:
            print('Payment for Airlines')
        elif 3300 <= int(mcc) < 3500:
            print('Payment for Car Rental')
        elif 3500 <= int(mcc) < 4000:
            print('Payment for Lodging')
        elif 4000 <= int(mcc) < 4800:
            print('Payment for Transportation Services')
        elif 4800 <= int(mcc) < 5000:
            print('Payment for Utility Services')
        elif 5000 <= int(mcc) < 5600:
            print('Payment for Retail Outlet Services')
        elif 5600 <= int(mcc) < 5700:
            print('Payment for Clothing Stores')
        elif 5700 <= int(mcc) < 7300:
            print('Payment for Miscellaneous Stores')
        elif 7300 <= int(mcc) < 8000:
            print('Payment for Business Services')
        elif 8000 <= int(mcc) < 9000:
            print('Payment for Professional Services and Membership Organizations')
        elif 9000 <= int(mcc) < 9999:
            print('Payment for Government Services')
        else:
            print('End of services')
        curs.execute('SELECT * FROM balance')
        res = curs.fetchall()
        total = res[curs.lastrowid - 2][5] - res[curs.lastrowid - 1][4]  # total(past) - debit(last) = total(now)
        curs.execute(f'UPDATE balance '
                     f'SET  total = ? '
                     f'WHERE id = ?',
                     (total, res[curs.lastrowid - 1][0]))
        curs.execute('SELECT * FROM balance')
        res = curs.fetchall()
        print(f'Your account balance: {res[curs.lastrowid - 1][5]}$')  # total
        if res[curs.lastrowid-1][5] <= 0:
            curs.execute(f'ROLLBACK')  # Отмена последней операции при отрицательном или нулевом балансе счета.
            print(tip4)
        else:
            conn.commit()
    except Error as er:
        print(f'{tip5}: {er}')
    except TypeError as er:
        print(er)
    else:
        conn.commit()


def set_credit():
    """Input value of gained money for 'TABLE'
        val = money INTEGER
        mcc = MCC(4-digits code) TEXT
        :return: TABLE with VALUES(
        id = AUTOINCREMENT,
        assignment = '0000',
        datetime = current date and time,
        credit = val,
        debit = 0,
        total = total(prev) + credit
        """
    mcc = '0000'
    while True:
        try:
            val = int(input(tip3))
        except ValueError:
            print('Must be an integer or not empty')
        else:
            break
    try:
        curs.execute(f'INSERT INTO balance (assignment, datetime, credit) VALUES (?,?,?)',
                     (mcc, date, val))
        curs.execute('SELECT * FROM balance')
        res = curs.fetchall()
        total = res[curs.lastrowid - 2][5] + res[curs.lastrowid - 1][3]  # total(past) + credit(last) = total(now)
        curs.execute(f'UPDATE balance '
                     f'SET total = ? '
                     f'WHERE id = ?',
                     (total, res[curs.lastrowid - 1][0]))
        curs.execute('SELECT * FROM balance')
        res = curs.fetchall()
        print(f'Your account balance: {res[curs.lastrowid - 1][5]}$')
    except Error as er:
        print(f'{tip5}: {er}')


def get_total_balance():
    """Return to console amount of money on your balance from table"""
    try:
        curs.execute('SELECT * FROM balance')
    except Error as er:
        print(f'{tip5}: {er}')
    else:
        res = curs.fetchall()
        print(f'Your account balance: {res[curs.lastrowid - 1][5]}$')


def get_total_expenses():
    """Return to console how much that you spent all the time"""
    debit = []
    try:
        curs.execute('SELECT debit '
                     'FROM balance '
                     'WHERE debit > 0')
    except Error as er:
        print(f'{tip5}: {er}')
    else:
        for dbt in curs.fetchall():
            for i in dbt:
                debit.append(i)
        print(f'All time expenses: {sum(debit)}$')


def get_period_expenses():
    """Return to console how much you spent in a particular month"""
    lst = []
    endwith_dct = {1: 'st', 2: 'nd', 3: 'rd'}
    while True:
        try:
            while True:
                month = int(input(tip6))
                if month <= 0 or month >= 13:
                    print('Uugh... Wrong period. Try again.')
                else:
                    break
        except ValueError:
            print('Must be an integer or not empty')
        else:
            break

    try:
        curs.execute('SELECT datetime, debit '
                     'FROM balance '
                     'WHERE debit > 0')
    except Error as er:
        print(f'{tip5}: {er}')
    else:
        for d in curs.fetchall():
            if int(d[0][5:7]) == month:
                lst.append(d[1])
        if month not in endwith_dct.keys():
            end = 'th'
        else:
            end = endwith_dct.get(month)
        if not lst:
            print(f'There is no records for the {month}{end} month')
        else:
            print(f'Your expenses for the {month}{end} month are {sum(lst)}$')


def cli():
    """Main interface"""
    tip = ("""
    [1] - Create new DB. (WARNING! If old DB exists it will be ERASED. Use this option carefully!)
    [2] - View balance (Просмотр баланса)
    [3] - View total debit (Просмотр расходов за все время)
    [4] - View debit for month (Просмотр расходов за конкретный месяц)
    [5] - Top up your balance (Пополнить счет)
    [6] - Withdraw money from an account (Оплата услуги)
    [q] - Exit
    
    Enter choice: """)
    done = False
    while not done:
        chosen = False
        while not chosen:
            try:
                choice = input(tip).strip()[0].lower()
            except (EOFError, KeyboardInterrupt, IndexError):
                choice = 'q'
            print('\nYou picked: [%s]' % choice)
            if choice not in '123456q':
                print('Invalid option, try again')
            else:
                chosen = True

        if choice == 'q':
            print('Thank you for your cooperation. Glory to Ukraine! Bye.')
            done = True
        if choice == '1':
            db_create()
        if choice == '2':
            get_total_balance()
        if choice == '3':
            get_total_expenses()
        if choice == '4':
            get_period_expenses()
        if choice == '5':
            set_credit()
        if choice == '6':
            set_debit()


if __name__ == '__main__':
    with sq.connect('balance.db') as conn:
    # with sq.connect(':memory:') as conn:  # Enable that for debugging
        curs = conn.cursor()
        # cli()  # MAIN INTERFACE
        # funcs to debugging
        # db_create()
        # set_debit()
        # set_credit()
        # get_total_balance()
        # get_total_expenses()
        # get_period_expenses().
        # tab_names()
